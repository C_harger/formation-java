package fr.casden.formation;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Utils2 <U>{
    public void tri(List<U> Liste, Comparator<U> comparator){
        Liste.sort(comparator);
    }

    public U recherche(List<U> liste,Predicate<U> predicate){
        for (U u : liste) {
            if (predicate.accept(u)) return u;
        }
        return null;
    }
}
