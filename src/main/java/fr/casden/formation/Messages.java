package fr.casden.formation;

import java.util.ResourceBundle;


public class Messages {
    private static final String BUNDLE_NAME = "fr.casden.formation.messages";
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);


    private Messages() {}

    public static final String getvalue(String key){
        return RESOURCE_BUNDLE.getString(key);
    }
}
