package fr.casden.formation;

public enum Legislation {
    DEFAULT,
    RGPD,
    US_SHIELD,
}
