package fr.casden.formation;

public class Produitnonselectexecption extends RuntimeException{
    
    public Produitnonselectexecption (String string) {
        super(string);
    }
    
    public void Nonselectproduit(){
        System.out.printf("Selectionner un produit\n");
    }
}
