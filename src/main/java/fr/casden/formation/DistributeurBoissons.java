package fr.casden.formation;

import java.util.Map;

public class DistributeurBoissons extends Distributeur{
    private String temperature;

    public DistributeurBoissons(Map<String, Produits> listeproduits) {
        super(listeproduits);

    }
    
    protected void distribuerproduit(){
        super.distribuerproduit();
        System.out.printf("Merci d'avoir pris une boisson\n");
        }

        public String getTemperature() {
            return temperature;
        }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    @Override
    public boolean fonctionnesurbatterie() {
        return true;
    }

    @Override
    public double niveaubatterie() {
        return 48;
    }

    @Override
    public String diagnostique() {
        return "Diag Distrib boissons : OK";
    }

}
