package fr.casden.formation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

public class App 
{
    public static void main( String[] args )
    {
        
        Hashtable<String,Produits> listeproduit =new Hashtable<String,Produits>(10);
        
        listeproduit.put("café",new Produits("Café",1d));
        listeproduit.put("thé",new Produits("Thé",1d));
        listeproduit.put("coca",new Produits("Coca",2d));
        listeproduit.put("bière",new Produits("Bière",3d));
        listeproduit.put("eau",new Produits("Eau",1d));

        //Déclaration nouveau distributeur de boisson (db) a partir de distributeur
        DistributeurBoissons db = new DistributeurBoissons(listeproduit);
        
        //Déclaration nouveau distributeur générique (d) a partir de distributeur de boisson
        Distributeur d = db;
/*
        try{        
            db.introduirepaiement(0.2d);
        } catch (Produitnonselectexecption e){
            e.printStackTrace();
            System.out.printf("Sélectionner boisson avant paiement\n");
        }

        try {
            db.choisirproduit("bière");
            db.introduirepaiement(5d);
        } catch (Produitindisponibleexeption e) {
            e.printStackTrace();
            System.out.printf("Prevenir maintenance\n");
        }
        
        db.introduirepaiement(0.5d);
        db.introduirepaiement(5d);

        try {
            d.choisirproduit("soda");
            db.introduirepaiement(5d);
        } catch (Produitindisponibleexeption e) {
            e.printStackTrace();
            //System.out.printf("Prevenir maintenance\n");
            e.PrevenirMaintenance();
        }

*/
        DistributeurBoissons db2 =new DistributeurBoissons(listeproduit);
        DistributeurBoissons db3 =new DistributeurBoissons(listeproduit);
        DistributeurFriandises df2 =new DistributeurFriandises(listeproduit);
        DistributeurFriandises df3 =new DistributeurFriandises(listeproduit);

        db.setTypelegislation(Legislation.US_SHIELD);

        Distributeurng dng =new Distributeurng();
        
        List<DistributeurBoissons> ldb = new ArrayList<DistributeurBoissons>(); 
        List<DistributeurFriandises> ldf = new ArrayList<DistributeurFriandises>();
        //List<Distributeur> ld = new ArrayList<Distributeur>();
        
        ldb.add(db);
        ldb.add(db2);
        ldb.add(db3);

        ldf.add(df2);
        ldf.add(df3);

        List<Distributeur> ld = Arrays.asList(db,db2,db3,df2,df3);

        List<Diagnostique> listediag = Arrays.asList(db,db2,db3,df2,df3,dng);

        for (DistributeurBoissons distributeurBoissons : ldb) {
            System.out.println(distributeurBoissons.diagnostique());
        }
        System.out.println("--------------------------------");
        for (DistributeurFriandises distributeurFriandises : ldf) {
            System.out.println(distributeurFriandises.diagnostique());
        }
        System.out.println("--------------------------------");
        for (Distributeur distributeur : ld) {
            System.out.println(distributeur.diagnostique());
        }
        System.out.println("--------------------------------");
        for (Diagnostique diag : listediag) {
            System.out.println(diag.diagnostique());
        }
        System.out.println("--------------------------------");
        System.out.println("--------------------------------");
        
        Iterator<Diagnostique> it = listediag.iterator();

        while(it.hasNext())
        {
            System.out.println(it.next().diagnostique());
        }
    } 


}

