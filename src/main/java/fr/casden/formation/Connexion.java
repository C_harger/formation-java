package fr.casden.formation;

import java.util.ArrayList;
import java.util.List;

public class Connexion {
    private Connexion() {};
    private static List<Connexion> listcnx = new ArrayList<Connexion>();
    private static Connexion cnx = null; 
    public static Connexion getIstance(){
        if (listcnx.size()<10) {
            Connexion cnx = new Connexion();
            listcnx.add(cnx);
        }
        else
        {
            throw new RuntimeException("Pool plein");
        }
        return cnx;
    }
}
