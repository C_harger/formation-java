package fr.casden.formation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Casdenauditable {
    String header() default "Audit en cours";
    Detailaudit detail() default Detailaudit.STANDARD;
}
