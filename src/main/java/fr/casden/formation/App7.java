package fr.casden.formation;

import java.sql.DriverManager;
import java.sql.SQLException;

public class App7 {
    public static void main(String[] args) {

        var url = "jdbc:h2:mem:";
        try (var con = DriverManager.getConnection(url)) {
            var stm = con.createStatement();
            stm.executeUpdate("CREATE TABLE Societaires(id INT PRIMARY KEY AUTO_INCREMENT,nom VARCHAR(255), age INT)");
            stm.executeUpdate("INSERT INTO Societaires(nom,age) VALUES('HARGER',50);");
            stm.executeUpdate("INSERT INTO Societaires(nom,age) VALUES('MARTIN',40);");
            stm.executeUpdate("INSERT INTO Societaires(nom,age) VALUES('ROBERT',80);");
            
            var rs = stm.executeQuery("SELECT * FROM Societaires");

            while (rs.next()){
                System.out.format("Societaire : %s age de %s s'appelle : %s\n",rs.getString("id"),rs.getString("age"),rs.getString("nom"));  
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
}
