package fr.casden.formation;

public enum Repas {
    LUNDI("Pizza"),
    MARDI("Choucroute"),
    MERCREDI("Raclette"),
    JEUDI("Kebab"),
    VENDREDI("Saumon"),
    SAMEDI("Couscous"),
    DIMANCHE("Hamburger");

    private String menudujour;
    
    public String getMenudujour() {
        return menudujour;
    }

    private Repas(String menudujour) {
        this.menudujour = menudujour;
    }
}
