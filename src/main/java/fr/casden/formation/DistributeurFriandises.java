package fr.casden.formation;

import java.util.Map;

public class DistributeurFriandises extends Distributeur{

    public DistributeurFriandises(Map<String, Produits> listeproduits) {
        super(listeproduits);

    }

    @Override
    public boolean fonctionnesurbatterie() {
        return false;
    }

    @Override
    public double niveaubatterie() {
        return 0;
    }

    @Override
    public String diagnostique() {
        return "Diag Distrib friandises : OK";
    }
    
}
