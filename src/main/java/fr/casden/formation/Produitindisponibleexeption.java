package fr.casden.formation;

public class Produitindisponibleexeption extends Exception{

    public Produitindisponibleexeption(String string) {
        super(string);
    }
    
    public void PrevenirMaintenance(){
        System.out.printf("Prevenir maintenance\n");
    }

}
