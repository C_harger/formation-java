package fr.casden.formation;

import java.util.Map;

public abstract class Distributeur implements Diagnostique{
    protected Produits produitselectionne;
    protected double sommeintroduite=0;
    private Map<String,Produits> listeproduits;
    private Legislation typelegislation = Legislation.DEFAULT;

    public Distributeur(Map<String,Produits> listeproduits) {
        this.listeproduits=listeproduits;
    }

    private void rendremonnaie(double monnaiearendre){
        if(monnaiearendre>0){
            System.out.printf("Récupérez votre monnaie : %,.2f\n",monnaiearendre);
        }
        sommeintroduite=0;
        produitselectionne=null;

        if (fonctionnesurbatterie() && (niveaubatterie()<50)){
            System.out.printf("Recharger la batterie \n");
        }
    }

     public void setTypelegislation(Legislation typeLegislation) {
        this.typelegislation = typeLegislation;
    }

    protected void distribuerproduit(){
        System.out.printf("Somme introduite : %,.2f €\n",sommeintroduite);
        Utils.println("Distribution de : " + produitselectionne.getProduit() + " au tarif de : " + produitselectionne.getTarif()+" €");
        enregtransac();
        }

    public String getProduit(){
        //return boisson.getBoisson();
        return produitselectionne.getProduit();
        }

	public void choisirproduit(String produitchoisi) throws Produitindisponibleexeption {
        if(listeproduits.containsKey(produitchoisi)){
            this.produitselectionne=listeproduits.get(produitchoisi);
            System.out.printf("Produit choisi : %s, merci d'introduire %,.2f €\n",produitselectionne.getProduit(),produitselectionne.getTarif());
        }
        else
        {
            System.out.printf("Produit inexistant\n");
            throw new Produitindisponibleexeption("!!! Produit inexistant !!!");
        }
	}

    public void introduirepaiement(double d) throws Produitnonselectexecption{
        sommeintroduite=sommeintroduite+d;
        if (produitselectionne==null){
            //System.out.printf("Sélectionner d abord un produit\n");
            rendremonnaie(sommeintroduite);
            throw new Produitnonselectexecption("!!! séctionner d abord un produit !!!");
        }
        if(sommeintroduite>=produitselectionne.getTarif()){
            distribuerproduit();
            rendremonnaie(sommeintroduite-produitselectionne.getTarif());
        }
        else
        {
            System.out.printf("Somme introduite : %,.2f €\n",sommeintroduite);
            System.out.printf("il faut ajouter %,.2f €\n",produitselectionne.getTarif()-sommeintroduite);
        }
    }

    private void enregtransac(){
        switch (typelegislation) {
            case DEFAULT:
                 System.out.printf("Transaction enregistrée en base de données\n");        
                 break;
            case US_SHIELD:
                System.out.printf("Transaction enregistrée en base de données aux USA\n");        
                break;
            case RGPD:
                System.out.printf("Transaction enregistrée en base de données en FRANCE\n");        
                break;
        }
        

    }

    public abstract boolean fonctionnesurbatterie();
    public abstract double niveaubatterie();
    public abstract String diagnostique();
    
}
 
