package fr.casden.formation;

public class Ascenseur implements Runnable{
    private String libelle;
    private int nbetage;
    public Ascenseur(String libelle, int nbetage) {
        this.libelle = libelle;
        this.nbetage = nbetage;
    }
    
    @Override
    public void run(){
        int etage=0;
        while (etage < nbetage) {
            try {
                Thread.sleep(Math.round(Math.random() *3 * 1000));
                System.out.format("%s à l'étage %s\n",libelle,etage);
            } catch(InterruptedException e) {
                    e.printStackTrace();
                    return;
            }
            etage++;
        }
        System.out.format("----------- %s arrivé au %s\n",libelle,etage);
    }
}