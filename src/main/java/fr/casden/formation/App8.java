package fr.casden.formation;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class App8 {

    public static void affichedatetime() {
        System.out.println(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
    }

    public static void main(String[] args) throws Exception {
        var job = new Job();

        var auditable = Job.class.getDeclaredMethod("dotraitement").getAnnotation(Casdenauditable.class);

        var isauditable = (auditable!=null);

        if (isauditable) {
            System.out.println(auditable.header());
            switch (auditable.detail())
            {
                case DETAILLE:
                case STANDARD:
                    System.out.println("Audit sans RGPD");
                    break;
                case DETAILLE_RGPD:
                case STANDARD_RGPD:
                    System.out.println("Audit avec RGPD");
                    break;
            }
            affichedatetime();
        }

        job.dotraitement();

        if (isauditable) {
            affichedatetime();
        }

    }
}
