package fr.casden.formation;

import java.lang.System.Logger.Level;

public class Job {
    @Casdenauditable(detail = Detailaudit.STANDARD )
    public boolean dotraitement() {
        try {
        System.getLogger(Job.class.getSimpleName()).log(Level.INFO,"Execution de mon traitement");
        Thread.sleep(2000);
        }
        catch(Exception ex) {
            return false;
        }
        return true;
    }
}
