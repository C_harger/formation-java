package fr.casden.formation;

public class Produits {
    private String libelle;
    private double tarif;
    
    public Produits(String libelle, double tarif){
        this.libelle=libelle;
        this.tarif=tarif;
    }

    public String getProduit(){
        return libelle;
    }
    public double getTarif(){
        return tarif;
    }

    public void distribuerproduit(){
        System.out.println("Distribution de : "+libelle+" "+tarif);
    }
    
}
