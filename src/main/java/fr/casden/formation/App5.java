package fr.casden.formation;

public class App5 {
    public static void main(String[] args)
    {
        System.out.println("Début Prog");

        Thread t1 = new Thread(new Ascenseur("Ascenseur 1",4));
        Thread t2 = new Thread(new Ascenseur("Ascenseur 2",5));

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Fin Prog");
    };
}  
